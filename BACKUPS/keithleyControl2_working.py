#!/usr/bin/python

import sys, time, math
from serial import *

class keithley(object):

    def __init__(self,path):
        #
        # Open port
        #
        #Syntax: serial.Serial(port=None, baudrate=9600, bytesize=EIGHTBITS, parity=PARITY_NONE,
        #                      stopbits=STOPBITS_ONE, timeout=None, xonxoff=False, rtscts=False,
        #                      writeTimeout=None, dsrdtr=False, interCharTimeout=None)
        self.port = Serial(path, baudrate=57600, bytesize=8, parity=PARITY_ODD, xonxoff=True)

    def init(self,path):
        #
        # Custom settings
        #

        # Reset Keithley
        self.port.write("*RST\r\n");
        # Disable hardware beeper
        self.port.write(":SYST:BEEP:STAT OFF\r\n");
        # Set voltage source to constant voltage output
        self.port.write(":SOUR:VOLT:MODE FIX\r\n");
        # Set output voltage to zero
        self.port.write(":SOUR:VOLT:IMM:AMPL 0\r\n");
        # Voltage output on rear panel plugs
        self.port.write(":ROUT:TERM REAR\r\n");
        # Set compliance to 100uA
        self.port.write(":CURR:PROT:LEV 100E-6\r\n")

    def setCompliance(self,complcurrent=100):

        # Set hardware compliance limit
        if not isInt(complcurrent):
            print "Wrong input format, only integer numbers allowed for compliance limit (in uA)"
        else:
            self.port.write(":CURR:PROT:LEV %sE-6\r\n" %(complcurrent) )

    def setVoltage(self,voltage=0):

        # Set bias voltage
        if not isInt(voltage):
            print "Wrong input format, only integer numbers allowed for voltage"
        else:
            self.port.write(":SOUR:VOLT:IMM:AMPL %s\r\n" %(-abs(voltage)) )
            self.port.write(":OUTP:STAT 1\r\n")

    def readCurrent(self):

        self.port.write(":READ?\r\n")
        line = self.port.readline()
	splitted = line.split(",")
        return splitted[1]

    def readVoltage(self):

        self.port.write(":READ?\r\n")
        line = self.port.readline()
        splitted = line.split(",")
        return splitted[0][1:]

    def reset(self):

        # Set output: off
        # Clear error messages
        # Load Presettings
        # Init custom settings

        self.port.write(":OUTP:STAT 0\r\n")
        self.port.write("*CLS\r\n")
        self.port.write(":SYST:PRES\r\n")
        self.init()

    def close(self):

        # Set output: off
        # Reset Keithley
        # Clear error messages
        # Load Presettings
        # Set "LOCAL" mode
        # Close connection

        self.port.write(":OUTP:STAT 0\r\n")
        self.port.write("*RST\r\n")
        self.port.write("*CLS\r\n")
        self.port.write(":SYST:PRES\r\n")
        self.port.write(":SYST:KEY 23\r\n")
        self.port.close()

    def help(self):

        print "\nKeithley 2410 Commandline Control Program"
        print "(c) 2015, IEKP Karlsruhe\n"

        print "-h \t Print this help page"
        print "-e \t Reset and close Keithley connection (exit)"
        print "-res \t Reset Keithley connection"
        print "-v x y=100 \t Enable output voltage, set to value -x and compliance limit to value y in uA (default: 100)"
        print "-rv \t Read voltage"
        print "-rc \t Read current"

def isInt(string):

    try:
        float(string)
        return True
    except ValueError:
        return False

def readMode(k):

    try:
        while True:
            current = k.readCurrent()
            print "   Current: " + current + " A"
            time.sleep(5)
    except KeyboardInterrupt:
        print "\n\nReadMode closed!\n"

def readModeBoth(k):

    try:
        while True:
            current = k.readCurrent()
            print "   Current: " + current + " A\n"
            voltage = k.readVoltage()
	    print "   Voltage: " + voltage + " V\n"
	    time.sleep(5)
    except KeyboardInterrupt:
        print "\n\nReadMode closed!\n"

# main loop
if __name__=='__main__':

    # Instanciate Keithly
    k0 = keithley('/dev/ttyUSB0')
    k1 = keithley('/dev/ttyUSB1')
    # Check command line arguments
    if (len(sys.argv) > 1):

        # Help page
        if (sys.argv[1] == "-h"):
            k0.help()
            sys.exit()

        # Set voltage and compliance
        elif (sys.argv[1] == "-v0"):
            k0.init()
            if ( (len(sys.argv)>3)):
                if isInt(sys.argv[3]):
                    k0.setCompliance(float(sys.argv[3]))
            k0.setVoltage(float(sys.argv[2]))

        # Set compliance limit
        elif (sys.argv[1] == "-c0"):
            k0.setCompliance(float(sys.argv[2]))

        # Read voltage and current
        elif (sys.argv[1] == "-rc0"):
            print k0.readCurrent()

        elif (sys.argv[1] == "-rv0"):
            print k0.readVoltage()

        # Reset
        elif (sys.argv[1] == "-res0"):
            k0.reset()
            sys.exit("Resetted Keithley device.")

        # Set voltage and compliance
        elif (sys.argv[1] == "-v1"):
            k1.init()
            if ( (len(sys.argv)>3)):
                if isInt(sys.argv[3]):
                    k1.setCompliance(float(sys.argv[3]))
            k1.setVoltage(float(sys.argv[2]))

        # Set compliance limit
        elif (sys.argv[1] == "-c1"):
            k1.setCompliance(float(sys.argv[2]))

        # Read voltage and current
        elif (sys.argv[1] == "-rc1"):
            print k1.readCurrent()

        elif (sys.argv[1] == "-rv1"):
            print k1.readVoltage()

        # Reset
        elif (sys.argv[1] == "-res1"):
            k1.reset()
            sys.exit("Resetted Keithley device.")

        # Exit
        elif (sys.argv[1] == "-e"):
            k0.close()
	    k1.close()	
            sys.exit("Resetted and closed Keithley connection. Goodbye.")

        # Exception
        else:
            sys.exit("Invalid command line parameter! Use -h for help")


    # Interactive loop
    else:

        # Set custom settings
        k0.init('/dev/ttyUSB0')
	k1.init('/dev/ttyUSB1')

        print "\nKeithley 2410 Interactive Control Program"
        print "(c) 2015, IEKP Karlsruhe\n"

        while(True):

            command = raw_input("Available commands: \nset voltage (int), set (c)ompliance, (r)ead Current, read Voltage (rv), enter ReadMode (rm, or rmb for both voltage and current), (res)et Keithley, (e)xit Program\n")

            if (command == "v0"):
                volt = raw_input('Enter voltage (in V): ')
                volt = float(volt)
		k0.setVoltage(volt)
                print "\n   Output voltage set to " + str(volt) + " V\n"

            elif (command == "c0"):
                compl = raw_input('Enter compliance limit (in uA): ')
                k0.setCompliance(compl)
                print "\n   Compliance limit set to " + compl + " uA\n"

            elif (command == "res0"):
                k0.reset()
                print "\n   Resetted Keithley device\n"

            elif (command == "r0"):
                current = k0.readCurrent()
                print "\n   Current: " + current + " A\n"

            elif (command == "rv0"):
                voltage = k0.readVoltage()
                print "\n   Voltage: " + voltage + " V\n"

            elif (command == "rm0"):
                print "\nEntering ReadMode (press Ctrl+C to exit)\n"
                readMode(k0)

            elif (command == "rmb0"):
                print "\nEntering ReadMode (press Ctrl+C to exit)\n"
		print "Both voltage and current are read\n"
                readModeBoth(k0)

            elif (command == "v1"):
                volt = raw_input('Enter voltage (in V): ')
                volt = float(volt)
		k1.setVoltage(volt)
                print "\n   Output voltage set to " + str(volt) + " V\n"

            elif (command == "c1"):
                compl = raw_input('Enter compliance limit (in uA): ')
                k1.setCompliance(compl)
                print "\n   Compliance limit set to " + compl + " uA\n"

            elif (command == "res1"):
                k1.reset()
                print "\n   Resetted Keithley device\n"

            elif (command == "r1"):
                current = k1.readCurrent()
                print "\n   Current: " + current + " A\n"

            elif (command == "rv1"):
                voltage = k1.readVoltage()
                print "\n   Voltage: " + voltage + " V\n"

            elif (command == "e"):
                k0.close()
		k1.close()
                sys.exit("\nConnection closed. Goodbye.\n")

            elif (command == "rm"):
                print "\nEntering ReadMode (press Ctrl+C to exit)\n"
                readMode(k)

            elif (command == "rmb"):
                print "\nEntering ReadMode (press Ctrl+C to exit)\n"
		print "Both voltage and current are read\n"
                readModeBoth(k)


            else:
                print "\nCommand not found!\n"

