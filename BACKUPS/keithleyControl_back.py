#!/usr/bin/python

import argparse
import sys, time, math
from serial import *

class keithley:

    def __init__(self,path='/dev/ttyUSB0'):
        #
        # Open port
        #
        #Syntax: serial.Serial(port=None, baudrate=9600, bytesize=EIGHTBITS, parity=PARITY_NONE,
        #                      stopbits=STOPBITS_ONE, timeout=None, xonxoff=False, rtscts=False,
        #                      writeTimeout=None, dsrdtr=False, interCharTimeout=None)
        self.port = Serial(path, baudrate=57600, bytesize=8, parity=PARITY_ODD, xonxoff=True)

    def init(self,path='/dev/ttyUSB0'):
        #
        # Custom settings
        #

        # Reset Keithley
        self.port.write("*RST\r\n");
        # Disable hardware beeper
        self.port.write(":SYST:BEEP:STAT OFF\r\n");
        # Set voltage source to constant voltage output
        self.port.write(":SOUR:VOLT:MODE FIX\r\n");
        # Set output voltage to zero
        self.port.write(":SOUR:VOLT:IMM:AMPL 0\r\n");
        # Voltage output on rear panel plugs
        self.port.write(":ROUT:TERM REAR\r\n");
        # Set compliance to 100uA
        self.port.write(":CURR:PROT:LEV 10000E-6\r\n")

    def setCompliance(self,complcurrent=100):

        # Set hardware compliance limit
        if not isInt(complcurrent):
            print "Wrong input format, only integer numbers allowed for compliance limit (in uA)"
        else:
            self.port.write(":CURR:PROT:LEV %sE-6\r\n" %(complcurrent) )

    def setVoltage(self,voltage=0):

        # Set bias voltage
        if not isInt(voltage):
            print "Wrong input format, only integer numbers allowed for voltage"
        else:
            self.port.write(":SOUR:VOLT:IMM:AMPL %s\r\n" %(-abs(voltage)) )
            self.port.write(":OUTP:STAT 1\r\n")

    def setVoltageSlow(self, stepsize, final_voltage):	
	print "\n Setting voltage to -"+ str(abs(final_voltage)) + "V in steps of "+ str(stepsize) + " V."
	for v in range(-1*abs(int(stepsize)),-1*(abs(int(final_voltage))+abs(int(stepsize))),-1*abs(int(stepsize))):
	    self.setVoltage(v)	
	    print "Voltage currently set to " + str(v) + "V."
	    time.sleep(10)	
	print "Voltage was slowly set to -" + str(abs(final_voltage))	
	
    def readCurrent(self):

        self.port.write(":READ?\r\n")
        line = self.port.readline()
        splitted = line.split(",")
        return splitted[1]

    def readVoltage(self):

        self.port.write(":READ?\r\n")
        line = self.port.readline()
        splitted = line.split(",")
        return splitted[0][1:]

    def reset(self):

        # Set output: off
        # Clear error messages
        # Load Presettings
        # Init custom settings

        self.port.write(":OUTP:STAT 0\r\n")
        self.port.write("*CLS\r\n")
        self.port.write(":SYST:PRES\r\n")
        self.init()

    def close(self):

        # Set output: off
        # Reset Keithley
        # Clear error messages
        # Load Presettings
        # Set "LOCAL" mode
        # Close connection

        self.port.write(":OUTP:STAT 0\r\n")
        self.port.write("*RST\r\n")
        self.port.write("*CLS\r\n")
        self.port.write(":SYST:PRES\r\n")
        self.port.write(":SYST:KEY 23\r\n")
        self.port.close()

    def help(self):

        print "\nKeithley 2410 Commandline Control Program"
        print "(c) 2015, IEKP Karlsruhe\n"

        print "-h \t Print this help page"
        print "-e \t Reset and close Keithley connection (exit)"
        print "-res \t Reset Keithley connection"
        print "-v x y=100 \t Enable output voltage, set to value -x and compliance limit to value y in uA (default: 100)"
        print "-rv \t Read voltage"
        print "-rc \t Read current"

def isInt(string):

    try:
        float(string)
        return True
    except ValueError:
        return False

def readMode(k):

    try:
        while True:
            current = k.readCurrent()
            print "   Current: " + current + " A"
            time.sleep(5)
    except KeyboardInterrupt:
        print "\n\nReadMode closed!\n"

# main loop
if __name__=='__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("--v", nargs=1, type=float, help="set voltage")
    parser.add_argument("--c", nargs=1, type=float, help="set compliance")
    parser.add_argument("--rc", action="store_true", help="read current")
    parser.add_argument("--rv", action="store_true", help="read voltage")
    parser.add_argument("--res", action="store_true", help="reset keitley")
    parser.add_argument("--e", action="store_true", help="exit program")	
    parser.add_argument("-p", metavar="", dest="p", nargs=1, default=["/dev/ttyUSB0"], help="Name of the USB device. Default is /dev/ttyUSB0")
    parser.add_argument("--sv", nargs=2, type=float, help="set voltage slowly")
 
    #parser.add_argument("-p", help="path to keithley USB port", action="store_const")

    args = parser.parse_args()

    # Instanciate Keithly
    k = keithley(args.p[0])
    # Check command line arguments
    if (len(sys.argv) > 3):

        # Set voltage and compliance
        if (args.v):
            #k.init()
            k.setVoltage(float(args.v[0]))

        # Set compliance limit
        elif (args.c):
            k.setCompliance(float(args.c[0]))

        # Read voltage and current
        elif (args.rc):
            print k.readCurrent()

        elif (args.rv):
            print k.readVoltage()

        # Reset
        elif (args.res):
            k.reset()
            sys.exit("Resetted Keithley device.")

        # Exit
        elif (args.e):
            k.close()
            sys.exit("Resetted and closed Keithley connection. Goodbye.")

	elif(args.sv):		
	    k.setVoltageSlow(args.sv[0],args.sv[1]) 				
        # Exception
        else:
            sys.exit("Invalid command line parameter! Use -h for help")

    # Interactive loop
    else:

        # Set custom settings
        k.init()

        print "\nKeithley 2410 Interactive Control Program"
        print "(c) 2015, IEKP Karlsruhe\n"

        while(True):

            command = raw_input("Available commands: \nset voltage (int), set voltage slowly (sv), set (c)ompliance, (r)ead Current, read Voltage (rv), enter ReadMode (rm), (res)et Keithley\n")

            if(isInt(command)):
                voltage = float(command)
                k.setVoltage(voltage)
                print "\n   Output voltage set to " + command + " V\n"

            elif (command == "c"):
                compl = raw_input('Enter compliance limit (in uA): ')
                k.setCompliance(compl)
                print "\n   Compliance limit set to " + compl + " uA\n"

            elif (command == "res"):
                k.reset()
                print "\n   Resetted Keithley device\n"

            elif (command == "r"):
                current = k.readCurrent()
                print "\n   Current: " + current + " A\n"

            elif (command == "rv"):
                voltage = k.readVoltage()
                print "\n   Voltage: " + voltage + " V\n"

            elif (command == "e"):
                k.close()
                sys.exit("\nConnection closed. Goodbye.\n")

            elif (command == "rm"):
                print "\nEntering ReadMode (press Ctrl+C to exit)\n"
                readMode(k)
	    elif(command == "sv"):
		step_size = int(raw_input("Enter the step size (in V):"))
		final_voltage = int(raw_input("Enter the output voltage to be set (in V): "))
		k.setVoltageSlow(step_size,final_voltage)
            else:
                print "\nCommand not found!\n"

