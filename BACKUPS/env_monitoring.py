import time, datetime, subprocess
from array import array

isTest = False

def getTimestamp():
	return str(time.time()).rstrip('\n')

def getVoltage(nr):
	if isTest:
		commandV = ["echo","300"]
	else:
		commandV = ["python", "keithleyControl.py",nr]
	pipe = subprocess.Popen(commandV, stdout=subprocess.PIPE)
	text = pipe.communicate()
	return text[0].rstrip('\n')


def getCurrent(nr):
	if isTest:
		commandV = ["echo","0.1"]
	else:
		commandV = ["python", "keithleyControl.py",nr]
	pipe = subprocess.Popen(commandV, stdout=subprocess.PIPE)
	text = pipe.communicate()
	return text[0].rstrip('\n')

def getTemperature():
	return "10"

def getHumidity():
	return "10"



if __name__=='__main__':

#def update_data_file(timestamp, temperature, humidity):

	environmentFile = open("environment.csv",'a')

	while True:

		ts = getTimestamp()
		v0 = getVoltage("-rv1")
		i0 = getCurrent("-rc1")
		v1 = getVoltage("-rv0")
		i1 = getCurrent("-rc0")
		temp = getTemperature()
		hum = getHumidity();

		environmentFile.write(str(ts)+","+str(temp)+","+str(hum)+","+str(v0)+","+str(i0)+","+str(v1)+","+str(i1)+","+"\n")
		print str(ts)+","+str(temp)+","+str(hum)+","+str(v0)+","+str(i0)+","+str(v1)+","+str(i1)+","+"\n"
		if isTest:
			break
		else:
			time.sleep(1)

