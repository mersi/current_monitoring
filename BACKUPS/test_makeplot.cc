#include <iostream>
#include <time.h>
#include <math.h>
#include "TTree.h"
#include "TCanvas.h"
#include "TH2D.h"
#include "data.h"
#include "TSystem.h"
#include "TText.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "data.h"
#include "TPad.h"

void prettify(TGraph *graph, float mSize, int mStyle, int mColor, const char* title, const char* xaxis, const char* yaxis){
	graph->SetMarkerSize(mSize);
	graph->SetMarkerStyle(mStyle);
	graph->SetMarkerColor(mColor);
	graph->SetLineColor(mColor);
	graph->SetTitle(title);
	graph->SetName(title);
	graph->GetXaxis()->SetTitle(xaxis);
	graph->GetYaxis()->SetTitle(yaxis);
}

int main(int argc, char **argv){

	
	double minTime_minutes=argc>1?atof(argv[1]):100.;
	gROOT->SetBatch(true);
	time_t now = time(NULL);

    	char buf[80];
        strftime(buf, sizeof(buf), "%Y-%m-%d.%X", localtime(&now));

	TTree *mmm = new TTree("data", "data");
	mmm->ReadFile("/afs/cern.ch/user/c/cmstkph2/public/environmental_monitoring/current_monitoring/currents.csv");

	data *tree = new data(mmm);

	TGraph* cur0 = new TGraph();
	TGraph* volt0 = new TGraph();
	TGraph *cur1=new TGraph();
	TGraph *volt1 = new TGraph();
	TGraph *hum = new TGraph();
	TGraph * temp = new TGraph();
	TMultiGraph *mgCur = new TMultiGraph();
	TMultiGraph *mgVolt = new TMultiGraph();


	int total = mmm->GetEntries()-1;
	for (Long64_t entry=total; entry>=0; entry--){
		tree->GetEntry(entry);
		double diffTime = (now - tree->timestamp_K0)/60.;
		if (diffTime>minTime_minutes) break;
		cur0->SetPoint(cur0->GetN(), (-now+tree->timestamp_K0)/60., tree->current0);
		volt0->SetPoint(volt0->GetN(), (-now+tree->timestamp_K0)/60., tree->voltage0);
		cur1->SetPoint(cur1->GetN(), (-now+tree->timestamp_K0)/60., tree->current1);
		volt1->SetPoint(volt1->GetN(), (-now+tree->timestamp_K0)/60., tree->voltage1);
		hum->SetPoint(hum->GetN(), (-now+tree->timestamp_K0)/60., tree->Humidity); 
		temp->SetPoint(temp->GetN(), (-now+tree->timestamp_K0)/60., tree->Temperature);	
	}//end for entries

	prettify(cur0, 0.5, 21, 1, "PS0: Current", "Minutes ago", "I[A]");
	prettify(cur1, 0.5, 21, 2, "PS1: Current", "Minutes ago", "I[A]");
	prettify(volt0, 0.5, 21, 1, "PS0: Voltage", "Minutes ago", "V[V]");
	prettify(volt1, 0.5, 21, 2, "PS0: Voltage", "Minutes ago", "V[V]");
	prettify(temp, 0.5, 21, 1, "Temperature", "Minutes ago", "T[#circC]");
	prettify(hum, 0.5, 21, 1, "Humidity", "Minutes ago", "Humidity[%]");
	mgCur->SetTitle("Currents");
	mgVolt->SetTitle("Voltages");

	TCanvas *c1 = new TCanvas(buf,buf,800,600);

	TText *t = new TText(0.5,0.5, buf);
   	t->SetTextAlign(22);
   	t->SetTextFont(43);
   	t->SetTextSize(10);
   	t->Draw();

	c1->Divide(2,2);
	c1->cd(1);
	mgCur->Add(cur0,"pl");
	mgCur->Add(cur1,"pl");
	mgCur->Draw("a");
	mgCur->GetXaxis()->SetTitle("Minutes ago");
	mgCur->GetYaxis()->SetTitle("I[A]");

	c1->cd(2);
	mgVolt->Add(volt0,"pl");
	mgVolt->Add(volt1,"pl");
	mgVolt->Draw("a");
	mgVolt->GetXaxis()->SetTitle("Minutes ago");
	mgVolt->GetYaxis()->SetTitle("V[V]");

	c1->cd(3);
	temp->Draw("apl");

	c1->cd(4);
	hum->Draw("apl");

	c1->SaveAs("/afs/cern.ch/user/g/gauzinge/www/beamtest/monitoring/Monitor.png");	
	//c1->SaveAs("/afs/cern.ch/user/j/jluetic/Monitor.png");	
	//c1->SaveAs("/afs/cern.ch/user/c/cmstkph2/public/environmental_monitoring/current_monitoring/plots/Monitor.pdf");

return 0;
}//end main int
