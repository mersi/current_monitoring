//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed May  4 15:23:42 2016 by ROOT version 5.34/36
// from TTree data/plots
// found on file: Memory Directory
//////////////////////////////////////////////////////////

#ifndef data_h
#define data_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class data {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Double_t        timestamp_K0;
   Double_t        voltage0;
   Double_t        current0;
   Double_t        timestamp_K1;
   Double_t        voltage1;
   Double_t        current1;
   Double_t        timestamp_TH;
   Double_t        Temperature;
   Double_t        Humidity;

   // List of branches
   TBranch        *b_timestamp_K0;   //!
   TBranch        *b_voltage0;   //!
   TBranch        *b_current0;   //!
   TBranch        *b_timestamp_K1;   //!
   TBranch        *b_voltage1;   //!
   TBranch        *b_current1;   //!
   TBranch        *b_timestamp_TH;   //!
   TBranch        *b_Temperature;   //!
   TBranch        *b_Humidity;   //!

   data(TTree *tree=0);
   virtual ~data();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef data_cxx
data::data(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
      if (!f || !f->IsOpen()) {
         f = new TFile("Memory Directory");
      }
      TDirectory * dir = (TDirectory*)f->Get("Rint:/");
      dir->GetObject("data",tree);

   }
   Init(tree);
}

data::~data()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t data::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t data::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void data::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("timestamp_K0", &timestamp_K0, &b_timestamp_K0);
   fChain->SetBranchAddress("voltage0", &voltage0, &b_voltage0);
   fChain->SetBranchAddress("current0", &current0, &b_current0);
   fChain->SetBranchAddress("timestamp_K1", &timestamp_K1, &b_timestamp_K1);
   fChain->SetBranchAddress("voltage1", &voltage1, &b_voltage1);
   fChain->SetBranchAddress("current1", &current1, &b_current1);
   fChain->SetBranchAddress("timestamp_TH", &timestamp_TH, &b_timestamp_TH);
   fChain->SetBranchAddress("Temperature", &Temperature, &b_Temperature);
   fChain->SetBranchAddress("Humidity", &Humidity, &b_Humidity);
   Notify();
}

Bool_t data::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void data::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t data::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
void data::Loop()
{
//   In a ROOT session, you can do:
//      Root > .L data.C
//      Root > data t
//      Root > t.GetEntry(12); // Fill t data members with entry number 12
//      Root > t.Show();       // Show values of entry 12
//      Root > t.Show(16);     // Read and show values of entry 16
//      Root > t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      // if (Cut(ientry) < 0) continue;
   }
}

#endif // #ifdef data_cxx
