#include <iostream>
#include <time.h>
#include <math.h>
#include "TTree.h"
#include "TCanvas.h"
#include "TH2D.h"
#include "data.h"
#include "TSystem.h"
#include "TText.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "data.h"
#include "TPad.h"
#include "TLegend.h"

void prettify(TGraph *graph, float mSize, int mStyle, int mColor, const char* title, const char* xaxis, const char* yaxis){
	graph->SetMarkerSize(mSize);
	graph->SetMarkerStyle(mStyle);
	graph->SetMarkerColor(mColor);
	graph->SetLineColor(mColor);
	graph->SetFillColor(0);
	graph->SetTitle(title);
	graph->SetName(title);
	graph->GetXaxis()->SetTitle(xaxis);
	graph->GetYaxis()->SetTitle(yaxis);
}

double dp(double hum, double temp){

	double b = 17.67;
	double c = 243.5;

	double gamma = log(hum/100)+(b*temp)/(c+temp);
	return c*gamma/(b-gamma);
}


int main(int argc, char **argv){

	
	double minTime_minutes=argc>1?atof(argv[1]):100.;
	gROOT->SetBatch(true);
	time_t now = time(NULL);

    	char buf[80];
        strftime(buf, sizeof(buf), "%Y-%m-%d.%X", localtime(&now));

	TTree *mmm = new TTree("data", "data");
	mmm->ReadFile("/afs/cern.ch/user/c/cmstkph2/public/environmental_monitoring/current_monitoring/currents.csv");

	data *tree = new data(mmm);

	TGraph* cur0 = new TGraph();
	TGraph* volt0 = new TGraph();
	TGraph *cur1=new TGraph();
	TGraph *volt1 = new TGraph();
	TGraph *hum = new TGraph();
	TGraph * temp = new TGraph();
	TGraph * dewpoint = new TGraph();
	TMultiGraph *mgCur = new TMultiGraph();
	TMultiGraph *mgVolt = new TMultiGraph();
	TMultiGraph *mgTemps = new TMultiGraph();


	int total = mmm->GetEntries()-1;
	for (Long64_t entry=total; entry>=0; entry--){
		tree->GetEntry(entry);
		double diffTime = (now - tree->timestamp_K0)/60.;
		if (diffTime>minTime_minutes) break;
		cur0->SetPoint(cur0->GetN(), (-now+tree->timestamp_K0)/60., tree->current0);
		volt0->SetPoint(volt0->GetN(), (-now+tree->timestamp_K0)/60., (-1)*tree->voltage0);
		cur1->SetPoint(cur1->GetN(), (-now+tree->timestamp_K0)/60., tree->current1);
		volt1->SetPoint(volt1->GetN(), (-now+tree->timestamp_K0)/60., (-1)*tree->voltage1);
		hum->SetPoint(hum->GetN(), (-now+tree->timestamp_K0)/60., tree->Humidity); 
		temp->SetPoint(temp->GetN(), (-now+tree->timestamp_K0)/60., tree->Temperature);	
		dewpoint->SetPoint(dewpoint->GetN(), (-now+tree->timestamp_K0)/60., dp(tree->Humidity,tree->Temperature));	
	}//end for entries

	prettify(cur0, 0.5, 21, 1, "PS0", "Minutes ago", "I[A]");
	prettify(cur1, 0.5, 21, 210, "PS1", "Minutes ago", "I[A]");
	prettify(volt0, 0.5, 21, 1, "PS0", "Minutes ago", "V[V]");
	prettify(volt1, 0.5, 21, 210, "PS1", "Minutes ago", "V[V]");
	prettify(temp, 0.5, 21, 95, "Temperature", "Minutes ago", "T[#circC]");
	prettify(hum, 0.5, 21, 51, "Humidity", "Minutes ago", "Humidity[%]");
	prettify(dewpoint, 0.5, 21, 61, "Dew point", "Minutes ago", "Dew point[#circC]");
	mgCur->SetTitle("Currents");
	mgVolt->SetTitle("Voltages");
	mgTemps->SetTitle("Temperatures");

	TCanvas *c1 = new TCanvas(buf,buf,1000,600);


	c1->Divide(3,2);
	c1->cd(1);
	mgCur->Add(cur0,"pl");
	mgCur->Add(cur1,"pl");
	mgCur->Draw("a");
	mgCur->GetXaxis()->SetTitle("Minutes ago");
	mgCur->GetYaxis()->SetTitle("I[A]");
	TLegend *leg = new TLegend(0.9, 0.9, 1., 1.);
	leg->SetLineColor(0);
	leg->AddEntry(cur0);
	leg->AddEntry(cur1);
	leg->Draw();

	c1->cd(2);
	mgVolt->Add(volt0,"pl");
	mgVolt->Add(volt1,"pl");
	mgVolt->Draw("a");
	mgVolt->GetXaxis()->SetTitle("Minutes ago");
	mgVolt->GetYaxis()->SetTitle("V[V]");
	TLegend *leg1 = new TLegend(0.9, 0.9, 1., 1.);
	leg1->SetLineColor(0);
	leg1->AddEntry(volt0);
	leg1->AddEntry(volt1);
	leg1->Draw();

	c1->cd(3);
	temp->Draw("apl");

	c1->cd(4);
	mgTemps->Add(temp,"pl");
	mgTemps->Add(dewpoint,"pl");
	mgTemps->Draw("a");
	mgTemps->GetXaxis()->SetTitle("Minutes ago");
	mgTemps->GetYaxis()->SetTitle("T[#circC]");
	TLegend *leg2 = new TLegend(0.6, 0.6, .8, .7);
	leg2->SetLineColor(0);
	leg2->AddEntry(temp);
	leg2->AddEntry(dewpoint);
	leg2->Draw();
	//dewpoint->Draw("apl");

	c1->cd(5);
	hum->Draw("apl");

	c1->cd(6);
	TText *t = new TText(0.5,0.5, Form("Plot created on: %s",buf));
   	t->SetTextAlign(22);
   	t->SetTextFont(43);
   	t->SetTextSize(17);
   	t->Draw();

	c1->SaveAs("/afs/cern.ch/user/g/gauzinge/www/beamtest/monitoring/Monitor.png");	
	//c1->SaveAs("/afs/cern.ch/user/j/jluetic/Monitor.png");	
	//c1->SaveAs("/afs/cern.ch/user/c/cmstkph2/public/environmental_monitoring/current_monitoring/plots/Monitor.pdf");

return 0;
}//end main int
